const CopyWebpackPlugin = require("copy-webpack-plugin");
const sass = require("node-sass");
const path = require("path");
const Promise = require("bluebird");
const fs = Promise.promisifyAll(require("fs"));

module.exports = {
  mode: "production",
  context: path.join(__dirname, "intermediate"),
  devtool: "source-map",
  entry: {
    index: "./index.ts",
    finance: "./finance.ts",
    price: "./price.ts",
    api: "./api.ts",
    "app/app": "./app/app.ts"
  },
  output: {
    path: __dirname + "/built",
    filename: "[name].js",
    chunkFilename: "[name].js"
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  externals: {
    "plotly.js": "Plotly"
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [{ loader: "ts-loader" }],
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: "**/*",
        ignore: ["*.ts", "*.js", "*.html", "*.scss"]
      },
      {
        from: "**/!(_*).scss",
        to: "[path][name].css",
        transform: (content, path) =>
          new Promise((resolve, reject) => {
            sass.render(
              {
                data: content.toString()
              },
              (error, data) => {
                if (error != null) {
                  reject(
                    new Error(
                      "SASS error [" +
                        path +
                        ":" +
                        error.line +
                        ":" +
                        error.column +
                        "]:\n" +
                        error.message
                    )
                  );
                } else {
                  resolve(data.css);
                }
              }
            );
          })
      },
      {
        from: "**/*.html",
        to: "[path][name].html"
      }
    ])
  ]
};
