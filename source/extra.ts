import * as lc from "laconic";
import * as moment from "moment";
import {
  uon,
  myGet,
  convert_from_base,
  format_decimal,
  currencyInfo,
  baseCurrency,
  basePath
} from "./shared";
import Big from "big.js";

const precision = (value, places) => {
  if (value >= 1) return value;
  return value.toPrecision(places);
};

const prep = new Array<() => Promise<void>>();
const typeHandlers = new Map<String, (v: any) => Promise<Element>>();

// Public interface
export const addPrep = (handler: () => Promise<void>) => {
  prep.push(handler);
};

export const addExtraHandler = (name, handler: (any) => Promise<Element>) => {
  typeHandlers.set(name, handler);
};

export const processExtra = async () => {
  for (let prepFunc of prep) {
    await prepFunc();
  }

  for (let extra of Array.prototype.slice.call(
    document.getElementsByClassName("extra")
  )) {
    const data = JSON.parse(extra.innerHTML);
    const handler = typeHandlers.get(data.type);
    const replacement = await handler(data);
    extra.replaceWith(replacement);
  }
};

// Price display + select
let rates: Map<String, number | string>;
let prices: { prices: Map<string, number> };

addPrep(async () => {
  const ratesP = myGet(basePath + "rates");
  const pricesP = myGet(basePath + "prices");
  rates = await ratesP;
  prices = await pricesP;
});

type priceArgs = { key: string; wrap?: string };

addExtraHandler("price", async (data: priceArgs) => {
  const currencies = ["usd", "eur", "jpy", "ltc"];
  const baseCurrencyInfo = currencyInfo.get(baseCurrency);
  const format_base = value => [
    ...baseCurrencyInfo.sym(),
    format_decimal(value.div(Math.pow(10, baseCurrencyInfo.majorPlaces)))
  ];

  const create_price = currency => {
    const price = prices.prices[data.key];
    const rate = rates[currency];
    return lc[data.wrap || "span"](
      { class: "value", style: "display: block; margin: 0;" },
      ...(uon(rate)
        ? format_base(new Big(price))
        : [
            ...currencyInfo.get(currency).sym(),
            precision(convert_from_base(currency, rates, price), 2) + "*"
          ])
    );
  };

  let priceEl = create_price("usd");
  const localCurrencyEls = [];
  for (let currency of currencies) {
    const el = lc.div(
      { class: localCurrencyEls.length == 0 ? "selected" : "" },
      currency.toUpperCase()
    );
    el.onclick = () => {
      const old_el = priceEl;
      priceEl = create_price(currency);
      old_el.parentNode.replaceChild(priceEl, old_el);
      localCurrencyEls.forEach(el2 => {
        if (el2 == el) el2.classList.add("selected");
        else el2.classList.remove("selected");
      });
    };
    localCurrencyEls.push(el);
  }
  return lc.div(
    { class: "price" },
    lc.div({ class: "sel" }, ...localCurrencyEls),
    priceEl
  );
});

addExtraHandler("price_notes", async (_: any) => {
  return lc.div(
    { class: "price_notes", style: "font-size: 0.8em" },
    "* Estimate based on current exchange rates. Conversion rates as of " +
      moment(rates["stamp"]).format("LLL") +
      ". Fees as of " +
      moment(prices["stamp"]).format("LLL")
  );
});
