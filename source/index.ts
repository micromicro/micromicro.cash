import { processExtra } from "./extra";

const adjustBg = () => {
  const title = <HTMLDivElement>document.getElementsByClassName("title")[0];
  const w = title.clientWidth;
  for (let bg of ["bg1", "bg2"]) {
    const bgw = 600;
    const bge = document.getElementById(bg);
    bge.style.backgroundPositionX =
      "" + (title.offsetLeft + w * 0.5 - bgw * 0.5) + "px";
    //document.body.style.backgroundSize = '' + (w * 1.75) + 'px auto'
  }
};
window.onresize = adjustBg;
setTimeout(adjustBg, 0);

processExtra();
