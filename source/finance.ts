import * as lc from "laconic";
import { addPrep, addExtraHandler, processExtra } from "./extra";
import { basePath, myGet } from "./shared";
import * as plotly from "plotly.js";
import * as moment from "moment";
import { convert_from_base } from "../intermediate/shared";

let finance: {
  stamp: number;
  future: number;
  data: {
    [id: string]: Array<{
      id: string;
      name: string;
      type: string;
      data: Array<number>;
    }>;
  };
};
let rates: Map<String, number | string>;

addPrep(async () => {
  const financeP = myGet(basePath + "finance");
  const ratesP = myGet(basePath + "rates");
  finance = await financeP;
  rates = await ratesP;
});

const convertY = (y: Array<number>) => {
  return y.map(price => convert_from_base("usd", rates, price));
};

type financeData = { title: string; key: string };
addExtraHandler("finance", async data_ => {
  const data = <financeData>data_;
  const div = lc.div();
  const traces = [];
  for (let series of finance.data[data.key]) {
    let sourceData =
      series.type == "money" ? convertY(series.data) : series.data;
    let useData = sourceData;
    if (data.key == "predict") {
      useData = useData.slice(0, useData.length - finance.future);
    }
    const axis = series.type == "money" ? "y2" : "y";
    {
      const x = [];
      for (let i = 0; i < useData.length; ++i) x.push(i);
      traces.push({
        x: x,
        y: useData,
        mode: "lines",
        name: series.name,
        yaxis: axis
      });
    }
    if (data.key == "predict") {
      const start = sourceData.length - finance.future - 1;
      let futureData = sourceData.slice(start, start + finance.future + 1);
      const x = [];
      for (let i = 0; i < futureData.length; ++i) x.push(start + i);
      traces.push({
        x: x,
        y: futureData,
        mode: "lines",
        name: "Estimate " + series.name.toLocaleLowerCase(),
        yaxis: axis
      });
    }
  }
  plotly.newPlot(div, traces, {
    title: data.title,
    legend: { orientation: "h" },
    yaxis: {
      title: "Count"
    },
    yaxis2: {
      title: "USD",
      side: "right",
      [<string>"overlaying"]: "y"
    }
  });
  const resize = () => {
    plotly.Plots.resize(div);
  };
  window.addEventListener("resize", resize);
  window.setTimeout(resize, 0);
  return div;
});

addExtraHandler("finance_notes", async (_: any) => {
  return lc.div(
    { class: "finance_notes", style: "font-size: 0.8em" },
    "Data as of " + moment(finance.stamp).format("LLL") + "."
  );
});

processExtra();
