<!-- 2018-06-12T08:40:00.000000+00:00 Finance and Fees -->

In our pursuit of operational and financial transparency, we've created a [dashboard](/help/finance.html?{bust}) with all the financial data used to calculate fees. Internally we use Litecoin, but for ease of reading everything has been converted to USD for display.

In addition, the fee table from the front page has been (mostly) moved to the [fees](/help/fees.html?{bust}) page with better explanations.
