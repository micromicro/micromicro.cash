<!-- Withdraw Money -->

To withdraw money you'll need to have a Litecoin wallet set up or an account at a service such as a Litecoin exchange.

### 1. Create a Litecoin receive address

In either your wallet software or the Litecoin service you use create a receive address.

### 2. Select `Withdraw` on the micromicro home screen

### 3. Scan the QR code or select `Paste` and enter the destination Litecoin address

When scanning a QR code, as soon as the code is recognized you will be taken to the next step. If you paste an address, press `Go` to continue to the next step.

### 3. Enter the amount you want to withdraw and press `Withdraw`

Note that you can't withdraw an amount that's a fraction of the minimum denomination of a Litecoin. The default transfer fee is deducted from the amount you enter.
