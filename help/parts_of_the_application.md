<!-- Parts of the Application -->

micromicro is pretty simple but it might be unclear how to use some of its features when you're starting. This guide will explain the general application organization and some of the more subtle features.

### 1. Home screen

![Home screen](parts_home_screen.jpg)

1.  Your username

2.  Your balance. If you use the account on multiple devices or have recently deposited money (and in some other situations) this might not be up to date. Press the `Refresh` button (8.) to get the latest number.

    Groups of three digits are separated by a space to make it easy to count them. By default it's displayed in Litecoin but you can change to an estimate in other currencies in `Settings` (9.).

3.  Send: Use this to send money to other micromicro accounts. See [Send money](/help/send_money.html?{{bust}}) for usage instructions.

4.  Receive: Press this button to receive money from other micromicro accounts. See [Receive money](/help/receive_money.html?{{bust}}) for usage instructions.

5.  List: List receive and send addresses you've created, recent addresses you've scanned or visited, and recent transactions to or from your account. You can delete receive and send addresses from the list (the money in a deleted send address returns to you).

6.  Deposit: Create a Litecoin address for depositing money into your account. You can use the address to withdraw money from an exchange directly into your account or to send money from wallet software. See [Deposit money](/help/deposit_money.html?{{bust}}) for usage instructions.

7.  Withdraw: Scan or paste a Litecoin address to withdraw money from your account. See [Withdraw money](/help/withdraw_money.html?{{bust}}) for usage instructions.

8.  Refresh: Refresh downloaded information such as your current balance and exchange rates.

9.  Settings: Change account settings, your preferred display currency, etc.

10. Log out: Log the current user out and return to the login screen.

### 2. Forms

micromicro uses data entry forms in several locations, such as when creating a receive or send address.

![Example form](parts_form.jpg)

micromicro forms have a number of unique features.

1.  <img alt="The plus" class="inline" src="/app/plus.svg">: This line in the form is optional. Press the <img alt="plus button" class="inline" src="/app/plus.svg"> to select it.

2.  <img alt="The minus" class="inline" src="/app/minus.svg">: This line in the form is optional and is currently selected. Press the <img alt="minus button" class="inline" src="/app/minus.svg"> to deselect it.

3.  <img alt="The question mark" class="inline" src="/app/help.svg">: Click on this button to toggle explanations for the lines in the form.

<br>
<br>
