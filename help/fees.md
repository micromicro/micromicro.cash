<!-- Fee Structure -->

micromicro takes a certain amount of money to run, for processing, data storage, network transfer, code updates, miscellaneous business costs, etc.

To cover these costs, various operations have a small surcharge and accounts are charged periodic maintenance fees.

## Receive

<p><script type="application/json" class="extra">{"type": "price", "key": "transaction"}</script> per transaction</p>

This is deducted from the amount sent before changing the receiver's balance. This is applied to transactions that have normal (deferred) accounting.

## Receive (fast)

<p><script type="application/json" class="extra">{"type": "price", "key": "fast_transaction"}</script> per transaction</p>

This is deducted from the amount sent before changing the receiver's balance. This is applied to transactions that are accounted immediately.

## Account Maintenance

<p><script type="application/json" class="extra">{"type": "price", "key": "account"}</script> per account or per send address, every week</p>

This is periodically deducted from each account and out address. New accounts have a grace period where no maintenance fee is applied, to give users a chance to deposit money. If an account past the grace period doesn't have enough balance, or an out address doesn't have enough remaining value to cover the maintenance fee it will be deleted.

## Create In Address

<p><script type="application/json" class="extra">{"type": "price", "key": "create_in"}</script> per address</p>

This is charged to the receiver's balance when they create a new receive address.

## Address Maintenance

<p><script type="application/json" class="extra">{"type": "price", "key": "create_in"}</script> per receive address, every week</p>

This is the same as the receive address creation fee; it's charged periodically to the receiver for each receive address that exists at the time of application. If an account doesn't have enough balance to cover the maintenance fees for its addresses the account will be deleted.

<script type="application/json" class="extra">{"type": "price_notes"}</script>

# Philosophy

In most cases the fee reflects the cost of whatever it's applied to - for example, if 80% of our monthly costs come from processing and storing transactions, the sum of money we collect from transaction surcharges should roughly cover that 80%.

Some fees also are intended to encourage services built on micromicro to use the api conservatively (to discourage creating lots of unused addresses, etc). These fees will not prevent vandals and rogue software from disrupting the system - in those cases throttling and other mitigation techniques will be employed.

# Dynamic pricing

The fees shown above are decided by a system that monitors usage and costs in real time and adjusts the fees to match.

To prevent it from going out of control we've set a hard upper limit on each of the fees - currently the cost of a fast (immediately accounted) transaction, the largest fee, will not go above $0.01 USD. There's also a lower limit - no fee can go below 0.01 of the smallest Litecoin denomination.

Starting out, we're still losing money but if our calculations are at all reasonable we should be able to cover our costs through collected fees alone, and if the service grows the fees may even decrease by an order of magnitude.

<script src="/price.js?{{bust}}" module></script>
