<!-- Send Money -->

There are two ways to send money within micromicro:

### 1. If you were given a QR code

1.  Press `Send`

2.  Scan the QR code

3.  Fill in the form and press `Send`

### 2. If you were given a link

1.  Open the link in your browser

2.  Fill in the form and press `Send`

### 2. If you weren't given a QR code or link

1.  Press `Send`.

2.  Click the `Create Address` tab at the top.

3.  Fill in the form including the amount of money you want to send.

4.  Press `Offer`

You can save the QR code as an image to print or attach to something. You can also use the text link below it to send in a text message, email, etc.
