<!-- Receive Money -->

There are two ways to receive money within micromicro:

### Create a `receive` address

1.  Press `Receive` and fill out the form. Press the question mark to see an explanation of each form field.

2.  Press `Receive` at the bottom.

Provide the QR code and/or the address to others to allow them to send you money.

### If you were provided a `send` QR code

1.  Press `Receive`

2.  Select the `Scan` tab at the top

3.  Scan the address QR code

4.  Confirm the details and press `Receive`

### If you were provided a link

1.  Open the link in your browser to receive it.

2.  Confirm the details and press `Receive`
