<!-- Why Litecoin -->

The choice between backing currencies wasn't easy and was evaluated on a number of criteria. The major candidates were

- USD
- Bitcoin
- Litecoin

with USD as the original preferred choice.

After a lot of research we came up with these pros and cons:

## 1. Interfacing with the banking network

- USD: Fundamentally difficult. Services that provide open access to the banking network have extensive TOS that frequently forbid money transfer services, and using any such service would be a risk. Direct access to the banking network is not generally available. Additionally, there may be extensive auditing and legal requirements.
- Bitcoin: Very easy - at a minimum just run a wallet.
- Litecoin: Very easy - at a minimum just run a wallet.

## 2. Onboarding users

- USD: Easy for US users. Interfacing with banks worldwide may be difficult, but if deposits by credit card are supported it may still be simple for consumers.

  Bank transfers to deposit/withdraw money may cost several dollars, but credit card transactions will likely be on the order of $0.30.

- Bitcoin: Users need to create an account at an exchange. The process isn't more difficult than creating a bank account, but it's still relatively uncommon.

  But Bitcoin transaction fees have recently been very high - if a new user wants to try micromicro they need to deposit a large amount of money. The Bitcoin development team hasn't adopted any measures to reduce costs.

- Litecoin: Like Bitcoin, users need to create an account at an exchange.

  However, Litecoin transaction fees have never grown to the extent Bitcoin fees have, peaking at a dollar or two per transaction. In addition, Litecoin's stated goal is low transaction fees and they have adopted multiple changes in this direction (block sizes and time, segwit, etc).

## 3. Currency stability

- USD: Very stable, costs for consumer goods don't change significantly.
- Bitcoin: Unstable.
- Litecoin: Unstable.

## 4. Other considerations

- USD: Both credit cards and bank transfers support some sort of "undo". The cost of supporting this would likely be significant, or we'd have to increase fees to cover losses undo events when they happen.
- Litecoin: Recently there have been risks of large pools switching currency to execute 51% attacks. Litecoin is fairly large, but measures may need to be taken.

### In the end, Litecoin

The interfacing issues make USD a non-starter. Cryptocurrency volatility is a concern, but hopefully it's not as critical for digital services and with time and adoption we hope that stability increases.

After that, being able to try micromicro without making a large investment was the most important factor. If you need to transfer Bitcoin to try micromicro, you'd have to deposit $10 or more and you'll still be losing 20% on transaction fees (depending on transaction volume). With Litecoin you can try it out for just $1 or $2.
