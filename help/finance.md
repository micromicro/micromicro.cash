<!-- Financial Data -->

All monetary values are in USD because that is the currency of our largest expenses.

<script type="application/json" class="extra">{"type": "finance", "key": "year", "title": "Past Year"}</script>

<script type="application/json" class="extra">{"type": "finance", "key": "predict", "title": "Estimation"}</script>

The fees are based on a prediction of costs and usage. The cost accounts are detailed below:

<script type="application/json" class="extra">{"type": "finance", "key": "accounts", "title": "Cost Accounts"}</script>

Some accounts are based on externally imposed costs (hosting), others are to support future development and will be zero until the income covers higher priority accounts.

<script type="application/json" class="extra">{"type": "finance_notes"}</script>

<script src="https://cdn.plot.ly/plotly-1.38.0.min.js"></script>
<script src="/finance.js?{{bust}}" module></script>
