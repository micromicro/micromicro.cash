<!-- Project Goals -->

micromicro development follows two fundamental rules:

### 1. Create a robust and simple API

micromicro was created from a dream to see the birth of other services that can't exist without microtransations.

Rather than create a comprehensive solution ourselves, we'd like to see a vibrant third party ecosystem with featureful client software, storefront creation services, financial assistants, etc.

To this end, micromicro aims to be a _minimal_ microtransaction service - a service that provides the basics that other services need to get started, provide those basics well, and no more.

### 2. Keep costs low

Money transfer services like micromicro are lucrative business. It's quite possible that micromicro could keep somewhat large margins on transaction fees and still gather a sizable userbase, but this has two disadvantages.

1.  It invites competitors, where competition could fracture the userbase leaving everyone worse off. If there's going to be competition it should be on some basis other than profits.

2.  Every price increment makes some businesses infeasible. Perhaps you could sell a useful service for at most $0.01-0.02. If the fees are $0.002 you can run your business, you make a living and improve your customers' lives. If the fees are $0.01 though, your prices are doubled (or your margin eliminated), the plan falls through, and the benefits of your service are lost to the world.
